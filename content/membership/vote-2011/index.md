---
title: "Eclipse Bylaw Changes 2011"
author: "Andrew Ross"
date: 2011-07-12 
keywords: ["membership", "vote 2011", "bylaws", "membership agreement"] 
---

**Voting concluded August 12, 2011** at 4pm EDT. The new Bylaw vote passed. The
results were:

- YES: 224
- NO: 5

Here are links to the new Bylaw document:

- [Summary of Bylaws changes](/org/documents/2011.06%20Members%20Meeting%20(Bylaws).pdf)
- [New Bylaws with change bars and comments](/org/documents/2011.05%20Eclipse%20Bylaws_commented.pdf)

The current [Bylaws of the Eclipse Foundation](/org/documents/Eclipse%20BYLAWS%202008_07_24%20Final.pdf) have
served our community well for a number of years. Changes have been proposed to
the Bylaws to prepare them for the future and to reflect current practice.
These changes require super-majority (two-thirds plus one) approval of the
Membership-at-Large to become effective.

The By-law changes have been unanimously approved by the board and were
outlined in the Annual General Members Meeting on March 21, 2011 and then
Members Meeting on June 21, 2011.

On **July 12, 2011** we initiated an electronic vote of the Eclipse Foundation
Membership-at-Large to approve the proposed changes. All Member Company
Representatives and Committers should vote on the proposed changes. If you are
a Member Company Represenative or Committer Member and did not receive your
official voting instructions on **July 12, 2011** please contact 
<membership@eclipse.org>.

## The proposed changes

- Replace the annual Roadmap with an annual community report
- Eliminate the Membership Committee. Make the IP Advisory Committee a Standing
  Committee
- Delete the Requirements Council
- Put the Architecture Council in charge of future revisions of the Eclipse
  Development Process
- Delete the requirement for there to be a ratio of Strategic Consumers to
  Strategic Developers
- Remove the requirement that the AGM be in Q1
- Allow for flexibility in covering expenses
- Sustaining Member board reps need to be a Sustaining Member
- Revisit collapsing votes from same member company to a single vote. Proposed
  solution: One committer one vote Total number of candidates from the same
  organization may not exceed one-half (1/2) of the total number of seats
  available for that year’s annual at-large election

## FAQ

Who do I contact for more information? For email questions, please contact
<membership@eclipse.org>. If you would like to speak to someone in North America
or APAC, please call Andrew Ross at +1 (613) 614-5772. In Europe and India,
please contact Ralph Müller at +49 (177) 449-0460.

How does the voting process work? All Committer Members and Company
Representatives are required to vote. Ballots have been sent electronically by
email. Committer Members are Committers who work for a Member Company or have
completed the Committer Membership paperwork. Each Company Representative vote
counts as one vote. Each group of committers for a Member Company also count as
a single vote based on a collapsable single vote. Each vote by a Committer who
has completed the Committer Membership paperwork and is not working for a
Member Company will also count as one vote. The voting process will be open for
30 days until close of business August 11 , 2011. A super-majority of
affirmative votes is required for the changes to become effective.

I do not see any of the benefits of membership noted in the Membership
Agreement or Bylaws; how do I find out about those? Members all have certain
rights and obligations as noted in the Bylaws and Membership Agreement.
However, many of the benefits members receive are from programs run by the
Eclipse Foundation - thanks to the support of its membership. You can find out
about these benefits on the [membership benefits](/membership/#tab-membership)
page and some of the special programs being run on the [special programs](/membership/special-programs/) 
page. New benefits and programs are always being added and you can find out
about those from your Member Newsletters, Members Meetings, and other
information feeds.

