---
title: "Research @ Eclipse Foundation"
cascade:
  # header
  header_wrapper_class: "header-research-bg-img small-jumbotron-subtitle"
  headline: ""
  jumbotron_class: "col-xs-24"
  custom_jumbotron_class: "research-jumbotron col-sm-24"
  custom_jumbotron: |
    <div class="featured-jumbotron-custom">
      <h1 class="featured-jumbotron-headline">
        Transforming research into <br> <span class="text-highlighted">open source innovation</span>
      </h1>
      <p>
        Empowering business-friendly open source innovation to build thriving
        communities, develop effective dissemination strategies, and accelerate
        research translation for publicly funded projects.
      </p>
      <div class="row margin-top-30">
        <div class="col-md-18 match-height-item-by-row margin-bottom-20">
          <div class="research-jumbotron-stats">
            <div class="stats-item">
              <span class="stat">10+</span>
              <span class="stat-name">Years</span>
            </div>
            <div class="stats-item">
              <span class="stat">730+</span>
              <span class="stat-name">Partners</span>
            </div>
            <div class="stats-item">
              <span class="stat">30+</span>
              <span class="stat-name">Research Projects</span>
            </div>
            <div class="stats-item">
              <span class="stat">35+</span>
              <span class="stat-name">Countries Represented</span>
            </div>
          </div>
        </div>
        <div class="research-jumbotron-ctas col-md-4 match-height-item-by-row">
          <a class="btn btn-primary btn-lg btn-pill margin-x-auto" href="#stay-connected">
            Contact us
          </a>
        </div>
      </div>
      <div class="research-jumbotron-bottom-area break-container margin-top-30">
        <div class="container">
          <p class="research-animated-text"><span class="text-primary">research &gt;</span> <span class="text-highlighted" aria-label="it's in our code">it's_in_our_code</span></p>
        </div>
      </div>
    </div>
  hide_call_for_action : true
  hide_sidebar: true
  hide_page_title: true
  hide_breadcrumb: true
  # metadata
  description: Business-friendly open source and open innovation underpins exploitation, community building and dissemination strategy for European projects.
  keywords: ["eclipse", "research"]
  seo_title_suffix : " | Research | The Eclipse Foundation"

  page_css_file: /public/css/research-styles.css
  date: 2019-09-10T15:50:25-04:00
  hide_page_title: true
  show_featured_story: false
  show_featured_footer: false
  container: "container-fluid overflow-hidden"
---

{{< pages/research/index >}}

{{< mustache_js template-id="research-projects-row" path="/js/src/templates/research/research-projects-row.mustache" >}}
