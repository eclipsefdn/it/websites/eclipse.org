---
title: "HAL4SDV"
date: 2024-04-01T00:00:00-00:00
project_logo: "/research/images/research/r-projects/hal4sdv.png"
tags: ["Automotive", "SDV", "Software Defined Vehicle", "electronics", "E/E", "embedded" , "systems", "Real Time", "unified architecture", "standardization", "cyber security", "safety", "open"]
homepage: "https://www.hal4sdv.eu/"
facebook: ""
linkedin: "https://www.linkedin.com/company/102425493/"
twitter: "https://x.com/HAL4SDV"
youtube: ""
funding_bodies: ["chips-ju"]
eclipse_projects: []
project_topic: "Automotive, SDV"
summary: "Hardware Abstraction Layer for a European Software Defined Vehicle approach"
---

HAL4SDV will focus on methods, technologies and processes to be used in series development of vehicles ready for market in 2030
onwards, extrapolating the expected dramatic developments in the constitutive areas of microelectronics, communication
technologies, software engineering and AI for the period after 2030. The following assumptions about abstract requirements as basis
for this period are: 1) Absolute data-centricity and complete code portability through appropriate hardware abstraction ensure
maximum functional flexibility, reusability, and minimum time-to-market; 2) Ensure efficient fusion of decentralized, heterogeneous
data streams for efficient high-performance processing using specialized, reliable hardware architectures; 3) Inside-out continuum,
i.e., there is no longer a distinction between inside and outside the vehicle, all resources in the vehicle are accessible from the outside,
and the vehicle has full access to all resources on the outside; 4) Unlimited horizontal scalability (between moving road users/
vehicles/infrastructure) and vertical scalability (e.g. vehicle sizes); 5)Real-time capability is guaranteed, adequate computational
models and suitable hardware/software or E/E architectures are used; 6) Vehicles are fully “liable”, and they are defensible against
cyber-attacks.
With this overall and long-term vision in mind, HAL4SDV will focus on the short-term objectives of specifying the hardware
abstraction layer, its standardisation, the development of appropriate tool chains and methodological aspects to achieve or regain
technological leadership in core areas. Needless to say, the latter may also have an impact on subsequent projects (dedicated to the
layers above the hardware abstraction) as well as on parallel development work in Europe, e.g. on chip design and fabrication,
mobility services and software ecosystems in general.


This project is running from April 2024 - Mars 2027.

---

## Consortium
* AGENCIA ESTATAL CONSEJO SUPERIOR DE INVESTIGACIO - Spain
* Alkalee - France
* ALMA MATER STUDIORUM - UNIVERSITA DI BOLOGNA - Italy
* AVL LIST GMBH - Austria
* AVL SOFTWARE AND FUNCTIONS GMBH - Germany
* BAYERISCHE MOTOREN WERKE AKTIENGESELLSCHAFT - Germany
* CARIAD SE	- Germany
* CEA - Commisariat à l’Energie Atomique et aux energies alternatives	- France
* CONTINENTAL AUTOMOTIVE TECHNOLOGIES GmbH	- Germany
* Critical Software	- Portugal
* DASSAULT SYSTEMES	- France
* Deal Comp Ltd	- Finland
* Eclipse Foundation Europe GmbH	- Germany
* ELEKTROBIT AUTOMOTIVE FINLAND OY	- Finland
* ELEKTROBIT AUTOMOTIVE GmbH	- Germany
* ETAS GmbH	- Germany
* FORD OTOMOTIV SANAYI ANONIM SIRKETI	- Turkey
* FZI FORSCHUNGSZENTRUM INFORMATIK	- Germany
* INFINEON TECHNOLOGIES AG	- Germany
* INSTITUTO SUPERIOR DE ENGENHARIA DO PORTO	- Portugal
* KARLSRUHER INSTITUT FUER TECHNOLOGIE	- Germany
* MERCEDES-BENZ AG	- Germany
* NXP Semiconductors	- Czechia
* NXP Semiconductors	- France
* NXP Semiconductors	- Netherland
* OULUN YLIOPISTO	- Finland
* POLITECNICO DI MILANO	- Italy
* POLITECNICO DI TORINO	- Italy
* RENAULT SAS	- France
* RESILTECH SRL	- Italy
* Robert Bosch GmbH	- Germany
* ROVIMATICA SL	- Spain
* STMICROELECTRONICS SRL	- Italy
* STTech GmbH	- Germany
* SYSGO AG	- Germany
* SYSGO S.A.S	- France
* TECHNISCHE UNIVERSITAET MUENCHEN	- Germany
* TECHNISCHE UNIVERSITEIT EINDHOVEN	- Netherland
* Tensor embedded GmbH	- Germany
* TTTECH AUTO AG	- Belgium
* TTTECH AUTO GERMANY GMBH	- Germany
* TTTECH COMPUTERTECHNIK AG	- Austria
* TTTechAuto Spain S.L.U.	- Spain
* UAB TERAGLOBUS	- Linuania
* UNIVERSITA DEGLI STUDI DI MODENA E REGGIO EMILIA	- Italy
* UNIVERSITY OF STUTTGART	- Germany
* VALEO COMFORT AND DRIVING ASSISTANCE	- France
* VIRTUAL VEHICLE RESEARCH GmbH	- Austria
* VSB - TECHNICAL UNIVERSITY OF OSTRAVA	- Czechia
* ZF FRIEDRICHSHAFEN AG	- Germany

