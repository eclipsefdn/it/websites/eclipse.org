---
title: "CyberNEMO"
date: 2024-10-01T00:00:00-00:00
project_logo: "/research/images/research/r-projects/cybernemo.png"
tags: ["Cloud-Edge continuum", "Cybersecurity", "Zero Trust Architecture", "Zero Trust Federated ML", "SASE", "Decision Support System (DSS)"]
homepage: "https://cybernemo.eu/"
facebook: ""
linkedin: "https://www.linkedin.com/in/he-cybernemo-project/"
twitter: "https://x.com/HE_cybernemo"
youtube: ""
funding_bodies: ["horizon-eu"]
eclipse_projects: []
project_topic: "Cloud-Edge/Security"
summary: "End-to-end Cybersecurity to NEMO meta-OS"
---

Horizon project 101070118 ΝΕΜΟ (Next Generation Meta OS) builds an IoT-Edge-Cloud continuum, in the form of an open-source,
flexible, adaptable, and multi-technology meta-Operating System. NEMO aims to unleash the power of Artificial Intelligence IoT to
increase European autonomy in data processing and lower CO2 footprint. Leveraging on consortium partners technological
excellence, along with clear business and exploitation strategies, CyberNEMO builds on top of NEMO to add end-to-end cybersecurity
and trust on IoT-Edge-Cloud-Data Computing Continuum.
CyberNEMO will establish itself as a paradigm-shift to support resilience, risk preparedness, awareness, detection and mitigation
within Critical Infrastructures deployments and across supply chains. To achieve technology maturity and massive adoption,
CyberNEMO will not “reinvent the wheel”, but leverage on existing by-design, by-innovation, and by-collaboration zero-trust
cybersecurity and privacy protection systems, and introduce novel concepts, methods, tools, testing facilities and engagement
campaigns to go beyond today’s state of the art and create sustainable innovation, already evident within the project lifetime.
CyberNEMO will offer end-to-end and full stack protection, ranging from a low level Zero-Trust Network Access layer up to a human
AI explainable Situation Perception, Comprehension & Protection (SPCP) framework and tools, collaborative micro-cervices Auditing,
Certification & Accreditation and a pan-European Knowledge Sharing, risk Assessment, threat Analysis and incidents Mitigation
(SAAM) collaborative platform. Validation and penetration testing will take place in 6 pilots including OneLab for integration, various

Critical Infrastructures (Energy, Water, Healthcare), media distribution, agrifood and fintech supply chain, along with their cross-
domain, cross-border federation.

Sustainability and adoption will be offered via the de-facto European Open source Eclipse Foundation ecosystem.


This project is running from October 2024 - September 2027.

---

## Consortium
* SYNELIXIS LYSEIS PLIROFORIKIS AUTOMATISMOU & TILEPI - Greece
* DNV AS - Norway
* THALES SIX GTS FRANCE SAS - France
* ENGINEERING - INGEGNERIA INFORMATICA SPA - Italy
* NETCOMPANY-INTRASOFT SA - Luxembourg
* SIEMENS SRL - Romania
* MAGGIOLI SPA - Italy
* SPACE HELLAS ANONYMI ETAIREIA SYSTIMATA KAI YPIRES - Greece
* CYBERETHICS LAB SRLS - Italy
* DIGITAL SYSTEMS 4.0 - Bulgaria
* UNPARALLEL INNOVATION LDA - Portugal
* TELEFONICA INNOVACION DIGITAL SL - Spain
* ORGANISMOS TILEPIKOINONION TIS ELLADOS OTE AE - Greece
* ASM TERNI SPA - Italy
* ENTERSOFT ANONYMI ETAIREIA PARAGOGIS KAI EMPORIA - Greece
* MEDITCINSKY UNIVERSITET-PLOVDIV - Bulgaria
* SORBONNE UNIVERSITE - France
* UNIVERSIDAD POLITECNICA DE MADRID - Spain
* RHEINISCH-WESTFAELISCHE TECHNISCHE HOCHSCHULE A - Germany
* PANEPISTIMIO PATRON - Greece
* UNIVERSIDAD CARLOS III DE MADRID - Spain
* ECLIPSE FOUNDATION EUROPE GMBH - Germany
* SPHYNX TECHNOLOGY SOLUTIONS AG  - Switzerland

