---
title: "TRISTAN"
date: 2023-01-01T00:00:00-00:00
project_logo: "/research/images/research/r-projects/tristan.png"
tags: ["RISC-V", "OpenSource", "Open-Source", "embedded processors", "AI/ML-driven applications", "mobile communication", "automotive power control", "processor peripherals"]
homepage: "https://tristan-project.eu/"
facebook: ""
linkedin: ""
twitter: ""
youtube: ""
funding_bodies: ["horizonEU", "chips-ju"]
eclipse_projects: []
project_topic: "RISC-V / OpenHW"
summary: "Together for RISc-V Technology and ApplicatioNs"
---

TRISTAN’S overarching aim is to expand, mature and industrialize the European RISC-V ecosystem so that it is able to compete with
existing commercial alternatives. This will be achieved by leveraging the Open-Source community to gain in productivity and quality.
This goal will be achieved by defining a European strategy for RISC-V based designs including the creation of a repository of industrial
quality building blocks to be used for SoC designs in different application domains (e.g. automotive, industrial, etc.). 

The TRISTAN approach is holistic, covering both electronic design automation tools (EDA) and the full software stack. The broad consortium will
expose a large number of engineers to RISC-V technology, which will further encourage adoption. This ecosystem will ensure a
European sovereign alternative to existing industrial players. 

The 3-year project fits in the strategy of the European Commission to
support the digital transformation of all economic and societal sectors, and speed up the transition towards a green, climate neutral
and digital Europe. This transformation includes the development of new semiconductor components, such as processors, as these
are considered of key importance in retaining technological and digital sovereignty and build on significant prior investments in
knowledge generation in this domain. 

Development strategies leveraging public research funding that exploit Open-Source have
been shown to boost productivity, increase security, increase transparency, allow better interoperability, reduce cost to companies
and consumers, and avoid vendor lock-ins. The TRISTAN consortium is composed of 46 partners from industry (both large industries
as well as SMEs), research organizations, universities and RISC-V related industry associations, originating from Austria, Belgium,
Finland, France, Germany, Israel, Italy, the Netherlands, Poland, Romania, Turkey and Switzerland.

This project is running from December 2022 - December 2025.

---

## Consortium

* NXP Semiconductors Germany GmbH - DE (coordinator)
* Technische Universität Graz - AT
* Robert Bosch GmbH - DE
* Sysgo SYSGO GmbH - DE
* NXP Semiconductors Austria GmbH & Co. KG - AT
* Technische Universität Darmstadt - DE
* Greenwaves Technologies - FR
* ST Microelectronics SRL - IT
* Eindgenössische Technische Hochschule Zürich - CH
* NXP Semiconductors France - FR
* Infineon Technologies AG - DE
* Antmicro SP Zoo - PL
* Commissariat a l’Energie Atomique et aux Energies Alternatives - FR
* Universita di Bologna - IT
* Synthara AG - CH
* Siemens Aktiengesellschaft Österreich - AT
* Leonardo – Societa per Azioni - IT
* Siemens Electronic Design Automation LTd. - IL
* MINRES Technologies GmbH - DE
* Thales DIS France SAS - FR
* Technolution BV - NL
* Fraunhofer Gesellschaft zur Förderung der angewandten Forschung e.V. - DE
* Interuniversitair Micro-electronica Centrum - BE
* Thales - FR
* ECLIPSE Foundation Europe GmbH - DE
* Politecnico di Torino - IT
* ST Microelectronics Grenoble 2 SAS - FR
* E4 Computer Engineering SPA - IT
* Semify e.U. - AT
* Technische Universität München - DE
* Irdeto BV - NL
* Accemic Technologies GmbH - DE
* Technische Universiteit Twente - NL
* Yonga Teknoloji Mikroelektronik Arastirma Gelistirme Tic. - TR
* Epos Embedded Core & Power Systems GmbH & Co. KG - DE
* Robert Bosch France SAS - FR
* Tampere University - FI
* Nokia OYJ - FI
* Cargotec Finland Oy - FI
* Codasip GmbH - DE