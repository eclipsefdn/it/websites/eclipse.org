---
title: "Research Projects"
date: 2019-04-01T13:30:00-00:00
outputs:
  - HTML
  - JSON
cascade:
  container: "container"
  hide_breadcrumb: false
---

{{< grid/section-container class=" padding-top-40 padding-bottom-40 margin-bottom-40" isMarkdown="false" >}}
    <h2 class="text-center margin-bottom-40">The Eclipse Foundation is a Partner in these Projects</h2>
    <div 
      class="featured-projects list-inline"
      data-is-static-source="true"
      data-types="projects"
      data-url="/research/projects/index.json"
      data-template-id="research-projects-grid"
    >
    </div>
{{</ grid/section-container >}}

{{< mustache_js template-id="research-projects-grid" path="/js/src/templates/research/research-projects-grid.mustache" >}}
