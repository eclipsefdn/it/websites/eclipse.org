---
title: Brief
date: 2022-10-04T15:22:11-04:00
description: Reasons to collaborate on open source software at Eclipse Foundation
categories: []
keywords: ['collaborate', 'open source', 'working groups', 'interest groups', 'fees']
slug: ""
aliases: []
hide_page_title: true
hide_breadcrumb: true
hide_sidebar: true
page_css_file: public/css/collaborations-styles.css
container: container-fluid collaborations-brief-container padding-bottom-60
layout: single
resources:
    - src: sections/*.md
---

{{< pages/collaborations/working-groups/brief/table_of_contents >}}

{{< grid/section-container class="row featured-section featured-section-dark featured-section-brief margin-bottom-30" isMarkdown="true" >}}

# 5 Reasons to Collaborate on Open Source Software at the Eclipse Foundation { .padding-bottom-20 }

## Why a Working Group at the Eclipse Foundation is a Great Alternative to an Association { .padding-bottom-20 }

Today, organisations of all types and sizes across Europe recognize that open collaboration is a strategic imperative for business success.

For many European organisations, the natural first step towards open collaboration is to create a new association or foundation that’s dedicated to their particular technology or industry focus area. Unfortunately, these siloed associations almost inevitably experience challenges that severely restrict, slow, and complicate open innovation among members.

The good news is there’s an alternative approach that offers considerable advantages: Creating a collaborative, open ecosystem — called a working group — at the Eclipse Foundation.

### Eclipse Foundation Members Have Easy Access to a “Foundation in a Box" { .h4 }

The Eclipse Foundation AISBL is the largest open source foundation in Europe in terms of projects, developers, and members. During its nearly 20 years of operation, the Eclipse Foundation has successfully created and nurtured hundreds of open source and open specification projects and communities. It has also provided a home for dozens of vendor neutral, open working group ecosystems for more than a decade.

Working groups at the Eclipse Foundation receive [services and benefits](/membership/) that allow them to openly collaborate and innovate in a much faster, easier, and more cost-effective way than they can in a standalone association or foundation. As a result, they have a much greater chance of success.

{{</ grid/section-container >}}

{{< grid/section-container isMarkdown="false" >}}
    {{< grid/div isMarkdown="true" >}}
## Here's a closer look at why a working group is a great alternative to a traditional association
    {{</ grid/div >}}
    {{< pages/collaborations/working-groups/brief/sections >}}
{{</ grid/section-container >}}

{{< grid/section-container class="margin-top-40" isMarkdown="true" >}}
## Jumpstart Open Source Innovation at the Eclipse Foundation { .h4 .margin-bottom-30 }

When you create a new association, your open source ecosystem is starting from nothing. However, when you create a new working group at the Eclipse Foundation, your open source ecosystem begins with a strong foundation of governance, processes, and infrastructure it can immediately leverage.


Since 2019, the number of working groups hosted at the Eclipse Foundation has nearly tripled. More than 190 Eclipse Foundation members are also members of working group ecosystems, and a growing number participate in multiple working groups.


### Affordable Fees Open the Door to All { .h5 }
The fees for Eclipse Foundation membership, and for working group membership, are based on annual corporate revenues, so it’s affordable for organisations of all sizes to join working groups. And each working group has the flexibility to tailor its charter to define the right set of membership levels, fees, and committees to support its goals.


### Learn More { .h5 }
To learn more about the benefits of setting up a working group at the Eclipse Foundation, [visit our website](https://www.eclipse.org/org/workinggroups/about.php) or [contact us today](mailto:collaborations@eclipse-foundation.org).


{{</ grid/div >}}