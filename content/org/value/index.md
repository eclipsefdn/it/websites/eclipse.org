---
title: "Open Innovation Community - Business Value"
headline: "The Home of Entrepreneurial Open Source"
subtitle: |
  Community driven. <br>
  Code first. <br> 
  <span class="text-primary">Commercially-friendly.</span>
jumbotron_class: "col-xs-24"
keywords: ["open source", "business", "technology", "strategy", "operational value", "strategic value", "financial value"]
hide_page_title: true
hide_sidebar: true
container: "container-fluid"
page_css_file: "/public/css/org/value.css"
---

{{< pages/org/value >}}
