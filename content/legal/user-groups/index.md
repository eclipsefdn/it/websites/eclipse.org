---
title: Guidelines for User Groups that Support Eclipse Foundation Projects and Working Groups
author: Wayne Beaton
keywords:
    - legal
    - documents
    - about
---

This document provides guidelines for user groups that are based on Eclipse Foundation projects and/or working groups. These user groups may leverage one or more Eclipse Foundation brands (e.g., Eclipse® , Jakarta® , or Tangle™ ).

A User Group must first sign the [Eclipse User Group Trademark License Agreement](eclipse-user-group-trademark-license-agreement-v1.0.pdf) before using the relevant brand. The agreement provides more detail about how the brand can be used.

* Any User Group must have a focus on an Eclipse Foundation open source project and/or a working group community. A User Group can be a separate organization, or a subgroup of a larger group.
* User Groups should promote free software both within the group and in relationships with other organizations.
* User Groups should make an effort to foster a positive message about both Eclipse and free software.
* It is necessary for any User Group to have a contact person who can act as a representative for the group and who accepts responsibility to ensure that the guidelines are followed. If the User Group has a president, then the president would be a good person to fill this role. For example, this person will be responsible to ensure that the User Group follows the requirements regarding how to use the Eclipse brand.
* User Group members are expected to follow the Eclipse Foundation [Community Code of Conduct](https://eclipse.org/org/documents/Community_Code_of_Conduct.php) at functions and interactions.

If a User Group is interested in having its own domain (e.g. eclipse.xx), then please contact the Eclipse Foundation to discuss if this is possible before registering the domain yourself.

User Groups must follow the [Guidelines for Eclipse Logos & Trademarks](/legal/logo-guidelines).

General questions or inquiries about starting a User Group or to discuss a specific matter in relation to a User Group, please email [license@eclipse-foundation.org](mailto:license@eclipse-foundation.org)

* * *

Based on GNOME User Group Guidelines and subject to the [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)
