---
title: Specification Non Assertion Covenant
author: Wayne Beaton
keywords:
    - specifications
    - non assert
---

April 13, 2020

To the extent you submit or otherwise make available to an Eclipse Foundation Specification Project (as that term is defined by the Eclipse Intellectual Property Policy) any ideas, concepts, methods or other information, you agree that you will not assert, based on such submissions, any intellectual property rights that are essential to any implementation of the submission, against the Eclipse Foundation, its contributors, or its licensees, with respect to any implementation of such Specification (as that term is defined by the Eclipse Foundation Specification Process).
To further clarify, such submissions include, but are not limited to, submissions made to any public communications channel such as an email list, forum, bug report, or GitHub issue submissions.
