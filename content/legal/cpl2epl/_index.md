---
title: "CPL to EPL Conversion"
author: "Mike Milinkovich"
keywords: ["legal", "documents", "about", "cpl", "epl", "conversion", "convert"]
is_deprecated: true
layout: "single"
---

As part of the formation of the Eclipse Foundation, the Eclipse community
migrated from the Common Public License (CPL) to the Eclipse Public License
(EPL). This conversion resulted in changes to quite a few legal documents on
our website. This page maintains the transition plan and the old versions of
the eclipse.org documents which reference the CPL.

{{< figure src="/images/osi-certified-60x50.gif" caption="The Eclipse Public License is an OSI-approved open source license." >}}

The Eclipse Foundation began the transition from the CPL to the EPL on
September 9th, 2004. It was substantially completed with the release of Eclipse
3.1 on June 28, 2005.

## CPL to EPL Transition

- CPL to EPL Transition Plan Frequently Asked Questions.\
  This FAQ answers some of the commonly asked questions about transition from
  the Common Public License (CPL) to the Eclipse Public License (EPL).
- CPL to EPL Transition Plan The detailed plan (.pdf) for transitioning from
  the Common Public License (CPL) to the Eclipse Public License (EPL).

## Agreements and Licenses

- [Eclipse.org Software User Agreement](/legal/cpl2epl/notice/) By downloading
  builds or accessing the CVS repository, you acknowledge that you have read,
  understood, and agree to be bound by the terms and conditions contained in
  this agreement. This agreement is used for projects that default to the
  Common Public License (CPL).

## Frequently Asked Questions

- [CPL Frequently Asked Questions](http://www.ibm.com/developerworks/opensource/library/os-cplfaq/index.html)
  Written by IBM and hosted at IBM’s developerWorks site, this FAQ answers some
  of the commonly asked questions about the Common Public License (CPL).

## Resources for Committers

- ["About" Template (CPL References)](/legal/cpl2epl/about/) This is a template
  for an "about.html" ("About") file that describes CPL-licensed content.
  Abouts contain legal documentation and are maintained in the CVS repository
  and subsequently included in builds in plug-in and other directories. They
  usually contain information about licensing and sometimes also information
  about any use of cryptography. By convention, every plug-in usually has an
  About. If all the content in a plug-in, directory or module can be
  correctly licensed under the CPL then this template can be used without
  modification. Any other About should be approved by the relevant PMC who
  can also assist committers with drafting an About.
- [Default Copyright and License](/legal/cpl2epl/copyright-and-license-notice/)
  Notice The standard copyright and license notice should appear in all source
  files where possible. Variations on this notice may be required (for example
  where a license other than the CPL or EPL governs the use of the file).
