---
title: "About File Template"
is_deprecated: true
related:
  links:
    - text: CPL About.html in plain HTML
      url: about.html
    - text: CPL to EPL conversion
      url: /legal/cpl2epl/
    - text: Guide to legal documents
      url: /projects/handbook#legaldoc
---

## About This Content

20th June, 2002

### License

Eclipse.org makes available all content in this plug-in ("Content"). Unless
otherwise indicated below, the Content is provided to you under the terms and
conditions of the Common Public License Version 1.0 ("CPL"). A copy of the CPL
is available at [http://www.eclipse.org/legal/cpl/cpl-v10.html](/legal/cpl/cpl-v10.html). For purposes of the
CPL, "Program" will mean the Content.

### Contributions

If this Content is licensed to you under the terms and conditions of the CPL,
any Contributions, as defined in the CPL, uploaded, submitted, or otherwise
made available to Eclipse.org, members of Eclipse.org and/or the host of
Eclipse.org web site, by you that relate to such Content are provided under the
terms and conditions of the CPL and can be made available to others under the
terms of the CPL.

If this Content is licensed to you under license terms and conditions other
than the CPL ("Other License"), any modifications, enhancements and/or other
code and/or documentation ("Modifications") uploaded, submitted, or otherwise
made available to Eclipse.org, members of Eclipse.org and/or the host of
Eclipse.org, by you that relate to such Content are provided under terms and
conditions of the Other License and can be made available to others under the
terms of the Other License. In addition, with regard to Modifications for which
you are the copyright holder, you are also providing the Modifications under
the terms and conditions of the CPL and such Modifications can be made
available to others under the terms of the CPL.
