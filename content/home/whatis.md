---
title: What is Eclipse?
date: 2022-01-06T19:53:39.000Z
description: ""
categories: []
keywords: []
slug: ""
toc: false
draft: false
lastmod: 2022-03-21T18:52:54.424Z
hide_sidebar: true
---



Many people know Eclipse as an Integrated Development Environment (IDE) for Java. However, Eclipse is far more than just a Java IDE.

## Eclipse is a Java IDE...


Eclipse is widely regarded as _the_ Java development environment. It has all the bells and whistles, including:

*   Language-aware editors, views, ...
*   Refactoring support
*   Integrated unit testing and debugging
*   Incremental compilation and build
*   Team development support
*   Out of the box support for CVS

## Eclipse is an IDE Framework...

*   Eclipse + Java Development Tools (JDT) = Java IDE
    *   First class framework for Java
    *   Language aware editor
    *   Refactoring, search
    *   Incremental build
    *   Integrated debugging
*   Eclipse + C/C++ Development Tools (CDT) = C/C++ IDE
    *   First class framework for C/C++
    *   Language aware editor
    *   Refactoring, search
*   Eclipse + PHP = PHP IDE
*   Eclipse + JDT + CDT + PHP = Java, C/C++, PHP IDE
*   ...

## Eclipse is a Tools Framework...

While Eclipse is an excellent platform for building integrated development environments, it is far more general than that; Eclipse is used as a platform for tools that are not specifically related to software development.

The Eclipse platform provides extensibility through Equinox, an implementation of the OSGi R4 specification; plug-ins make Eclipse whatever you need it to be. Tools extend the Eclipse platform using plug-ins:

*   Business Intelligence and Reporting Tools (BIRT)
*   Data Tools Platform ([DTP](/datatools/))
*   Test and Performance Tooling Project (TPTP)
*   Web Tools Project (WTP)

Numerous frameworks are provided by Eclipse projects for building tools, including:

*   Eclipse Communications Framework ([ECF](/ecf/))
*   Graphical Editing Framework ([GEF](/gef/))
*   Eclipse Modeling Framework ([EMF](/emf/))
*   Graphical Modeling Framework ([GMF](/gmf/))

## Eclipse is a Application Framework...

If you exclude the the plug-ins that make Eclipse an IDE, including Java language support, debugging and team development support, you're left with a comprehensive general application framework that supports a rich user experience. The Eclipse Rich Client Platform (RCP) is being used today by many open source and commercial products to provide robust, scalable, enterprise-ready applications.

*   Support for multiple platforms: Linux, Windows, Mac OSX, UNIX, embedded
*   Rich widget set, graphics
*   Native-OS integration (drag and drop, OLE/XPCOM integration)
*   A platform for rich clients

## Eclipse is an Open Source Project...

The "Eclipse" project--responsible for the Eclipse SDK--is just one of many projects. Eclipse projects are managed, have schedules and plans, and ship real software. The focus of Eclipse projects is to create generally useful frameworks and APIs, along with exemplary projects.

There are nine (9) top level projects:

*   Eclipse Project
*   Tools
*   Web Tools Platform
*   Test & Performance Tools Platform
*   Business Intelligence and Reporting Tools
*   Data Tools Platform
*   Device Software Development Platform
*   SOA Tools Platform
*   Technology (Incubators)

In addition to the top level projects, there are dozens of sub projects.

## Eclipse is a Community...

All Eclipse projects are available for free download. No registration is required; downloads are not tracked and there are no sales calls. A large proportion of the community are Eclipse users: people who use Eclipse every day to get their job done.

Eclipse contributors number in the hundreds. There are many different kinds of contributions: bug reports, feature requests, patches, code, and design. A smaller number of committers are responsible for deciding (and actioning) what contributions will be integrated into the project's code base.

Hundreds of plug-ins are provided by commercial companies, organisations, and individuals.

There are numerous community-supported sources of information about Eclipse:

*   Planet Eclipse (www.planeteclipse.org)
*   EclipseZone (http://eclipsezone.com)
*   EclipseWiki (http://eclipsewiki.editme.com)
*   Eclipse Plug-in Central (http://eclipseplugincentral.com)
*   Others (http://eclipse.org/community)

50 million download requests to date.

## Eclipse is an Eco-System...

The Eclipse Eco-system is composed of more than 140 member companies, including major Java, Linux and Embedded vendors like BEA, Borland, JBoss, IBM, SAP, RedHat, Novell, Wind River, Mentor, ENEA, and QNX. These member companies contribute developers and resources to Eclipse projects and then use the output of those projects for their commercial software offerings.

Eclipse is focused on nurturing the eco-system to complement, and enhance the Eclipse Platform.

## Eclipse is a Foundation...

The Eclipse Foundation was created to manage and direct the ongoing development of the Eclipse open source software project. It is responsible for providing IT infrastructure required by development teams. The Eclipse Foundation is an independent not-for-profit Foundation formed in 2004.

## Eclipse is all these things...

*   A Java IDE
*   An IDE Framework
*   A Tools Framework
*   An Application Framework
*   An Open Source Enabler
*   A community
*   An eco-system
*   A foundation
