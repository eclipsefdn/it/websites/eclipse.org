---
title: "Security Resources"
date: 2024-11-18T15:50:25-04:00
tags: ["case studies", "social media kit", "surveys", "white papers", "resources"]
headline: "Security Resources"
custom_jumbotron:
  <div class="fw-300 fs-lg">
    Below are some useful links to resources about Eclipse Security.
  </div>
hide_page_title: true
header_wrapper_class: "security-page"
custom_jumbotron_class: "col-xs-24 fw-400"
container: "security-resources-page container"
draft: true # Set to false once we have more resources to display.
---

## Jump to a Section 

- [Case Studies](#case-studies)
- [White Papers](#white-papers)
- [Market Reports](#market-reports)

{{< newsroom/resources
  wg="security"
  title="Case Studies"
  type="case_study"
  template="image-with-title" 
  limit="100"
  class="col-sm-6"
>}}

<!--

---

{{< newsroom/resources
  wg="security"
  title="White Papers"
  type="white_paper"
  template="image-with-title" 
  limit="100"
  class="col-sm-6"
>}}

---

{{< newsroom/resources
  wg="security"
  title="Market Reports"
  type="market_report"
  template="image-with-title" 
  limit="100"
  class="col-sm-6"
>}}

{{< grid/div class="margin-y-30 text-center" isMarkdown="false" >}}
  {{< bootstrap/button href="#" >}}Back to Top{{</ bootstrap/button >}} 
{{</ grid/div >}}
