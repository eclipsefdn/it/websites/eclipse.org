---
title: Home
headline: The Community for Open Innovation and Collaboration
tagline: The Eclipse Foundation provides our global community of individuals and
  organisations with a mature, scalable, and business-friendly environment for
  open source software collaboration and innovation.
hide_page_title: true
redirect_url: /home/
hide_sidebar: true
hide_breadcrumb: true
show_featured_story: true
date: 2004-02-02T18:54:43.927Z
layout: single
links:
  - - href: /projects/
    - text: Discover Projects
  - - href: /collaborations
    - text: Industry Collaborations
  - - href: /membership/
    - text: Members
  - - href: /org/value
    - text: Business Value
container: container-fluid
lastmod: 2022-04-29T14:50:37.859Z
header_wrapper_class: header-default-bg-img
description: The Eclipse Foundation provides our global community of individuals and
  organisations with a mature, scalable, and business-friendly environment for
  open source software collaboration and innovation.
keywords:
  - eclipse
  - project
  - plug-ins
  - plugins
  - java
  - ide
  - swt
  - refactoring
  - free java ide
  - tools
  - platform
  - open source
  - development environment
  - development
  - ide
  - eclipse foundation
---

This page is not available on production. Redirecting to homepage...
