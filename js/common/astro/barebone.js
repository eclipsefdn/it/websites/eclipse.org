/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
*/

import 'jquery';
import 'bootstrap';

import eclipsefdnHeader from 'eclipsefdn-solstice-assets/js/solstice/astro/eclipsefdn.header';
import eclipsefdnSidebar from 'eclipsefdn-solstice-assets/js/solstice/astro/eclipsefdn.sidebar';
import eclipsefdnMegaMenuPromoContent from 'eclipsefdn-solstice-assets/js/solstice/eclipsefdn.mega-menu-promo-content.js';

const renderAstro = () => {
  eclipsefdnHeader.run();
  eclipsefdnSidebar.run();
  eclipsefdnMegaMenuPromoContent.render();
};

// If the document has already loaded, run and render the widgets. Otherwise,
// if the document has not yet loaded, run it on the DOMContentLoaded event.
if (document.readyState === 'complete' || document.readyState === 'loaded') {
  renderAstro();  
} else {
  document.addEventListener('DOMContentLoaded', renderAstro);
}
