/**
 * Copyright (c) 2024 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

/** 
  * Retrieves the innerHTML of a static html document's body.
  *
  * @param {string} path - The path to the static html document.
  * @returns {Promise<string>} A promise which resolves to the innerHTML of the static html
  * document.  
  * @throws
  */
const getBodyContent = async path => {
    const response = await fetch(path);
    const text = await response.text();

    const htmlDocument = new DOMParser().parseFromString(text, 'text/html');
    if (!htmlDocument.body) throw TypeError('No body element has been found in ' + path);

    return htmlDocument.body.innerHTML;
};

export default getBodyContent;