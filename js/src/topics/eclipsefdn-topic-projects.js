/*
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import defaultTemplate from '../templates/tpl-project-basic-card.mustache';
import renderTemplate from 'eclipsefdn-solstice-assets/js/solstice/templates/render-template';
import { getProjectsData } from 'eclipsefdn-solstice-assets/js/api/eclipsefdn.projects';

const apiBaseUrl = 'https://projects.eclipse.org/api';

const templates = {
  default: defaultTemplate,
}

const defaultOptions = {
  templateId: 'default',
  pageSize: 3,
}

/**
  * The render function for the projects of a given topic.
  */
const render = async () => {
  const element = document.querySelector('.eclipsefdn-topic-projects');
  if (!element) return;

  const options = { ...defaultOptions, ...element.dataset };
  
  // Assert that technology types has been provided.
  if (!options.technologyTypes) {
    console.error('No technology type(s) provided');
    return;
  }

  // Assert that pageSize can be converted into a number.
  try {
    Number.parseInt(options.pageSize)
  } catch (_error) {
    console.error('Could not convert pageSize to a number')
    return;
  }

  // We will maintain a project set to eliminate duplication of projects.
  let projectsSet = new Set();
  
  // Add projects from specified technology type
  try {
    let projectsFromType = await getProjectsData(`${apiBaseUrl}/projects?technology_types=${options.technologyTypes}`);
    projectsFromType.forEach((project) => projectsSet.add(project));
  } catch (_error) {
    console.error(`Could not fetch projects from given technology type: ${options.technologyTypes}`);
  }

  // Add projects from specified industry collaborations 
  if (options.industryCollaborations) {
    try {
      let projectsFromCollaborations = await getProjectsData(`${apiBaseUrl}/projects?industry_collaboration=${options.industryCollaborations}`);
      projectsFromCollaborations.forEach((project) => projectsSet.add(project));
    } catch (_error) {
      console.error(`Could not fetch projects from given industry collaboration: ${options.industryCollaborations}`);
    }
  }

  // Convert the set into an array. Sort it. And limit the number of projects
  // displayed.
  const projects = Array
    .from(projectsSet)
    .sort(randomComparator)
    .slice(0, Number.parseInt(options.pageSize));

  // Render the template with the project data
  await renderTemplate({
    element,
    templates,
    templateId: options.templateId,
    data: {
      items: projects,
    },
  });
};

/**
  * Randomly sorts elements in an array
  *
  * @param {*} _a - The first element for comparison 
  * @param {*} _b - The second element for comparison
  *
  * @returns A random shuffle between _a and _b. It does not matter what _a or
  * _b is. The result will always be random.
  */
const randomComparator = (_a, _b) => {
  return 0.5 - Math.random();
}
export default { render };
