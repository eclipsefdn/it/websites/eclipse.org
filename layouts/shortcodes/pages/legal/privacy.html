<!--
  Copyright (c) 2024 Eclipse Foundation, Inc.

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License v. 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
-->
<p>15 February 2024</p>
<p>
  This Privacy Policy explains how the Eclipse Group (&ldquo;Us&rdquo;, &ldquo;Our&rdquo;, or &ldquo;We&rdquo;, as
  further identified below in
  section &ldquo;Who We Are. Data Controllers and Our contact data&rdquo;) collects and processes Personal Information
  (as defined below) from website
  visitors (&ldquo;you&rdquo; or &ldquo;your&rdquo;). All references to the &ldquo;Website&rdquo; in this Privacy
  Policy are deemed to refer to
  any Eclipse Group website or online resource that contains a reference or link to this Privacy Policy,
  including in the first place the Website at <a href="/">https://www.eclipse.org/</a>
  as well as any other Website of the Eclipse Group.
</p>
<p>
  The Eclipse Group acts as the steward of Our open source projects, industry collaborations including working
  groups and interest groups, community, and ecosystem. We are committed to respecting your rights to privacy
  within Our global ecosystem. We promise to transparently share all aspects of how Eclipse Group projects and
  Websites work in regards to privacy, terms, and Personal Information, and We are in full support of efforts to
  ensure your protection online, regardless of what laws are applicable and what your location may be. We
  recognize that different laws apply to protect your privacy, including but not limited to the laws of the
  United States, Canada, European Union, and We will aim to apply the same standards of privacy and protection
  of Personal Information to all individuals.
</p>

<h2 class="h3" id="collection">The Personal Information We collect about you and why</h2>
<p>
  We collect and process your Personal Information only for the purposes set out in this Privacy Policy. As a
  platform for open source innovation and collaboration, your Personal Information can be submitted to Us, or
  collected by Us, in different ways.
</p>
<p>
  &ldquo;Personal Information&rdquo; is any information or data that relates to an individual which would enable that
  individual to be directly or indirectly identified. We may handle, notably, the following Personal
  Information about you:
</p>
<ol>
  <li>
    Contact/identification details (e.g. name, e-mail address, picture);
  </li>
  <li>
    Technical data (e.g. IP address, user behavior, web cookies); and
  </li>
  <li>
    Records of your contributions to Our open source projects and/or industry collaborations under the
    applicable specific agreements with Us.
  </li>
</ol>
<p>
  We may collect, store and use your Personal Information above for the following purpose(s):
</p>
<ol type="i">
  <li>To send you newsletters or other direct marketing communications that you have requested;</li>
  <li>To improve the Website and services;</li>
  <li>To deliver the content of the Website, to ensure the functionality of Our IT systems, and to improve the
    optimization of the Website;</li>
  <li>To contact you for reasons related to the service you have signed up for or to provide information you have
    requested;</li>
  <li>To provide you with the services that you requested;</li>
  <li>To enable you to store the data you request Us to store on Eclipse Group servers through services, including but
    not limited to, the Eclipse User Storage Service (USS);</li>
  <li>To provide access to the Website and services offered on the Website;</li>
  <li>To enable collaborative open source software development (e.g., to validate and maintain permanent records of
    authorship related to your contributions);</li>
  <li>To record your contributions to public communication channels that We manage or host (e.g., postings to a forum
    or a mailing list); and</li>
  <li>To prevent the misuse of the Website and the services and to detect fraudulent activities.</li>
</ol>
<p>
  When we collect, store and use your Personal Information for those purpose(s), it is and will remain in connection
  with and subject to, either (a) your acceptance, (b) the performance of Our services to you as a member or employee
  of a member or (c) the pursuance of Our own interests or the service to Our community.
</p>
<p>In some cases, We may not be able to provide Our services if you fail to provide to us with some Personal
  Information.</p>

<h2 class="h3" class="sharing">How We share your Personal Information and who We share it with</h2>
<p>We will not sell your Personal Information to any third party.</p>
<p>We will not disclose your Personal Information, except as described in this Privacy Policy.</p>
<p>Firstly, your Personal Information may be shared among the Eclipse entities within the Eclipse Group.</p>
<p>Secondly, We rely on contractually bound third party companies and external service providers (referred to as
  &ldquo;Processors&rdquo;) in order to provide Our services, and to improve the Website and Our services. In such
  cases, Personal Information may be shared with these Processors in order to allow them to continue providing their
  services. The Processors are permitted to use the Personal Information only for the purposes specified by Us.
  Furthermore, they are contractually obligated to handle your Personal Information exclusively in accordance with
  this Privacy Policy and in line with the applicable data protection laws. For example, your Personal Information may
  be disclosed to: </p>
<ul>
  <li>Data analytics providers;</li>
  <li>Communication services providers;</li>
  <li>IT service providers for the provision of hardware and software and for the implementation of maintenance work;
  </li>
  <li>Third-party contractors and consultants employed by Us who act as part of Our staff in conjunction with Our
    fulltime and part time employees</li>
  <li>If you are employed by, or a consultant of an Eclipse Member Company and have interacted with Eclipse Group or
    Our projects, We may provide your name and/or email address to your employer.</li>
</ul>
<p>Thirdly, We may disclose your Personal Information where:</p>
<ul>
  <li>We are required to do so by applicable law, by a governmental body or by a law enforcement agency;</li>
  <li>To establish or exercise Our legal rights or defend against legal claims; and</li>
  <li>To investigate, prevent or take actions against illegal activities, suspected fraud, situations involving
    potential threats to the physical safety of any person, violations of Our terms of use, or as otherwise required
    by law.</li>
</ul>

<h2 class="h3" id="who-we-are">Who We Are. Data controllers and Our contact data</h2>
<p>
  The Eclipse Group is comprised of our four corporate entities, namely Eclipse Foundation AISBL, Eclipse
  Foundation Europe GmbH, Eclipse.org Foundation, Inc. and Eclipse Foundation Canada, which together are jointly
  responsible as joint data controllers within the meaning of data protection legislation (the &ldquo;Eclipse
  Group&rdquo;).
  The Eclipse Group has a joint controllership agreement in place among the four corporate entities to document
  how the respective tasks and responsibilities in the processing of Personal Information are structured and who
  fulfills which data protection obligations. For more information about the joint controllership agreement in
  place, please contact Us at <a href="mailto:privacy@eclipse.org">privacy@eclipse.org</a>.
</p>
<p>
  Eclipse Foundation AISBL is your contact point. You may contact us either via postal mail to &ldquo;Eclipse
  Foundation AISBL, Rond Point Schuman 11, 1040 Brussels, Belgium&rdquo; or via e-mail to <a
    href="mailto:privacy@eclipse.org">privacy@eclipse.org</a>.
  Contact details of all four companies may be found <a
    href="https://www.eclipse.org/org/foundation/contact.php">here</a>. For more information about these companies,
  please contact <a href="mailto:emo@eclipse.org">emo@eclipse.org</a>.
</p>
<p>
  You can also assert your rights with regard to the processing of your Personal Information in joint
  controllership vis-à-vis Us as jointly responsible. In case you contact Us, We will coordinate in
  accordance with Our joint controllership agreement in order to respond to your inquiry and to guarantee
  your rights as a data subject.
</p>

<h2 class="h3" id="duration-kept">How long We keep your Personal Information</h2>
<p>
  Unless We are subject to other obligations such as legally mandated
  retention periods or ongoing court proceedings, We store your Personal
  Information only as long as necessary to carry out a service you have
  requested or to which you have given your consent, or as necessary to
  fulfill Our responsibilities as an open source foundation. As examples, We
  will store your Personal Information to enable you to access and use the
  Website and services as long as you have a registered ID account, and We
  will store your Personal Information related to your behavior on the
  Website, including your IP address, for up to 90 days in Our active files,
  and in Our log files used for security and system integrity purposes for as
  long as one year.
</p>

<h2 class="h3" id="international-transfers">International Transfers</h2>
<p>
  To enable Us to conform to GDPR, note that your Personal Information may be processed in countries both inside
  and outside the European Union (&ldquo;EU&rdquo;) or European Economic Area (&ldquo;EEA&rdquo;), including areas
  which may be regarded
  as having a lower level of data protection than the EU. In such cases, We ensure that a consistent level of
  protection for your Personal Information is guaranteed, including, under applicable EU rules, via the
  existence of an adequacy decision or the use of appropriate data transfer tools, for example, entering into
  the standard contractual clauses issued by the European Commission with other entities of the Eclipse Group or
  with Our Processors, all in order to ensure that they provide a sufficient level of protection for your
  Personal Information.
</p>

<h2 class="h3" id="our-commitment">Our Commitment to You</h2>
<p>
  We commit to providing you with the following rights in accordance with applicable laws:
</p>
<ol type="a">
  <li>to provide you with access to Personal Information that We hold about you;</li>
  <li>to update or correct any out-of-date or incorrect Personal Information that We hold about you;</li>
  <li>where the processing is based on your consent, the ability to withdraw consent at any given time, without
    affecting the lawfulness of processing based on consent before its withdrawal;</li>
  <li>to have your Personal Information erased by Us, unless it cannot be done due to Our requirement to fulfill Our
    responsibilities, including those as an open source foundation;</li>
  <li>to restrict processing where the conditions of article 18 of the EU&rsquo;s GDPR have been met;</li>
  <li>to support your request with respect to data portability;</li>
  <li>to respond to objections by you with respect to Our processing of your Personal Information; and</li>
  <li>to opt out of any direct marketing communications that We (with your consent) may send you.</li>
</ol>
<p>
  You can exercise these rights at any given time. We will respond within 30 days to your requests. Please
  contact <a href="mailto:privacy@eclipse.org">privacy@eclipse.org</a> to make such a request.
</p>
<p>
  If you are a resident of the European Union, you have the right to lodge a complaint, under EU data privacy
  rules, with the Data Protection Authority, by post to 35 Rue de la Presse, B-1000 Brussels, by e-mail to
  <a href="mailto:contact@apd-gba.be">contact@apd-gba.be</a>, or by telephone at
  <a href="tel:+3222744800" aria-label="+ 3 2. 2. 2 7 4. 4 8. 0 0.">+32 2 274 48 00</a>.
</p>

<h2 class="h3" id="information-security-and-quality">Information Security and Quality</h2>
<p>
  We protect the integrity of your Personal Information with appropriate technical and organizational measures, such
  as using encryption for transmission of certain forms of information, to help keep it secure, accurate, current, and
  complete.
</p>
<p>
  Your Personal Information may be stored in Canada, Switzerland, the European Union and the United States, always
  with Our same high security standards.
</p>

<p>
  It must be noted that the following Personal Information will be publicly available in unencrypted form and without
  access restriction:
</p>

<ul>
  <li>
    Any Personal Information collected by virtue of your contributions, including but not limited to your
    contributions to Our forums, public mailing lists, source code repositories, etc., which are subject to
    specific agreements you must enter into with Us (for example, the
    <a href="/legal/eca">Eclipse Contributor Agreement (ECA)</a>) prior to you
    contributing;
  </li>
  <li>
    Any data you store with Eclipse Group through Our services including, but not limited to, the
    User Storage Service (USS). We strongly advise you to not use these services to store passwords, any
    Personal Information, any confidential business information, or anything else that you do not want publicly
    available.
  </li>
</ul>
<p>
  Should you identify yourself as working for an Eclipse Member when you
  create your Eclipse account with Us, then we may disclose to the Member
  Representative of your organisation (as that term is defined in our Bylaws)
  any Personal Information collected by virtue of your contributions.
</p>

<h2 class="h3" id="cookies">Cookies</h2>
<p>
  We make use of cookies when you visit the Website, including both cookies we create as well as third party cookies
  including but not limited to Hubspot, Twitter, YouTube, and Google Analytics. These cookies are placed on your
  computer or other device and perform various functions. Some of these cookies are necessary for the operation of
  the Website. Other cookies are subject to your permission and they can help distinguish you from other visitors to
  Our websites, improve the user experience, measure traffic, collect and measure certain statistics, and deliver
  relevant ads.
</p>

<h2 class="h3" id="business-relationships">Business Relationships</h2>
<p>
  This Website contains links to other websites controlled or operated by parties other than the Eclipse Group.
  We are not responsible for the privacy practices or the content of such websites.
</p>

<h2 class="h3" id="notification-of-changes">Notification of Changes</h2>
<p>
  This Privacy Policy was last updated on 15 February 2024. A notice will be posted on
  <a href="/">www.eclipse.org</a> for thirty (30) days whenever this Privacy Policy is changed.
</p>

<h2 class="h3" id="questions">Questions Regarding This Policy</h2>
<p>
  Questions regarding this Privacy Policy should be directed to:
  <a href="mailto:privacy@eclipse.org">privacy@eclipse.org</a>.
</p>